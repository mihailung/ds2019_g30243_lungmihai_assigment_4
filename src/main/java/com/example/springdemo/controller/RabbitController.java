package com.example.springdemo.controller;

import com.example.springdemo.RabbitMQ.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;

@RestController
public class RabbitController {
    @Autowired
    Producer producer;

    @GetMapping("/send")
    public String sendMessage(){
              String line ="";
        try (BufferedReader br = new BufferedReader(new FileReader("activity.txt"))) {

            while ((line = br.readLine()) != null) {
                //Sending line to queue
                System.out.println(" [x] Sent '" + line + "'");
                producer.produceMsg(line);
                //System.out.println(" [x] Sent '" + line + "'");
            }
        }
        catch (Exception exe)
        {
            exe.printStackTrace();
        }

        return "Successfully MSG Sent";
    }
}
