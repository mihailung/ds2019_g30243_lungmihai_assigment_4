package com.example.springdemo.controller;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Controller
public class PatientController {

    @Autowired
    PatientRepository patientRepository;

    @RequestMapping(path = "/patients/edit/{id}")
    public String editPatient(Model model, @PathVariable("id") int id){
        Optional<Patient> patient = patientRepository.findById(id);
        model.addAttribute("patient", patientRepository.findById(id));
        return "patient-editor";
    }

    @RequestMapping(path = "/patients/create")
    public String createPatient(Model model){
        model.addAttribute("patient", new Patient());
        //patientRepository.save(patient);
        return "patient-creator";
    }

    @RequestMapping(path = "/patients/save", method = RequestMethod.POST)
    public String savePatient(Patient patient)
    {
        patientRepository.save(patient);
        return "success";
    }

    @RequestMapping(path = "/delete/{id}")
    public String deletePatient(Model model, @PathVariable("id") int id){
        patientRepository.deleteById(id);
        return "success";
    }

    @RequestMapping(path = "/patients/get")
    //@ResponseBody
    public String getAll(Model model){
        List<Patient> patients = new ArrayList<>();
        patientRepository.findAll().forEach(patients :: add);
        model.addAttribute("patients", patients);
        return "list-patients";
    }



}