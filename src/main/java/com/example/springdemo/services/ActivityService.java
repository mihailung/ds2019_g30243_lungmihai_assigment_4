package com.example.springdemo.services;

import com.example.demo.Activity;
import com.example.springdemo.entities.ActivityEntity;
import com.example.springdemo.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class ActivityService {


    private final ActivityRepository activityRepository;

    @Autowired
    public  ActivityService( ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Optional<ActivityEntity> findActivity(int id) {

        return activityRepository.findById(id);
    }
}
