package com.example.springdemo.services;

import com.example.springdemo.entities.Medication;

public interface MedicationService {
     Medication getMedication();
}