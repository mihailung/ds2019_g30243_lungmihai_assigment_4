package com.example.springdemo.services;
import com.example.demo.Activity;
import com.example.demo.ActivityDetailsRequest;
import com.example.demo.ActivityDetailsResponse;
import com.example.springdemo.entities.ActivityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.example.springdemo.repositories.ActivityRepository;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Optional;


@Endpoint
public class ActivityEndpoint
{
    private static final String NAMESPACE_URI = "https://www.howtodoinjava.com/xml/school";

    private ActivityRepository ActivityRepository;

    @Autowired
    public ActivityEndpoint(ActivityRepository ActivityRepository) {
        this.ActivityRepository = ActivityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ActivityDetailsRequest")
    @ResponsePayload
    public ActivityDetailsResponse getActivity(@RequestPayload ActivityDetailsRequest request) throws ParseException, DatatypeConfigurationException {

        ActivityDetailsResponse response = new ActivityDetailsResponse();

        Optional<ActivityEntity> activity = ActivityRepository.findById(request.getId());

        Activity act = new Activity();
        if (activity.isPresent()) {
            //
            act.setName(activity.get().getName());

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(activity.get().getStart_time());
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            //
            act.setStartTime(xmlCal);

            cal.setTime(activity.get().getEnd_time());
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            //
            act.setEndTime(xmlCal);
        }

        response.setActivity(act);

        return response;
    }
}
