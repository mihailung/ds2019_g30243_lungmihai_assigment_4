package com.example.springdemo;

import com.example.springdemo.RabbitMQ.Producer;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.TimeZone;

@SpringBootApplication
public class SpringDemoApplication {

@Autowired
Producer producer;

	@Bean
	MedicationService medicationService() {
		return new MedicationServiceImpl();
	}


	@Bean
	RmiServiceExporter exporter( MedicationService medicationService) {

		// Expose a service via RMI. Remote obect URL is:
		// rmi://<HOST>:<PORT>/<SERVICE_NAME>
		// 1099 is the default port

		Class<MedicationService> serviceInterface = MedicationService.class;
		RmiServiceExporter exporter = new RmiServiceExporter();
		exporter.setServiceInterface(serviceInterface);
		exporter.setService(medicationService);
		exporter.setServiceName(serviceInterface.getSimpleName());
		exporter.setRegistryPort(1099);
		return exporter;
	}



	public static void main(String[] args) {

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(SpringDemoApplication.class, args);
	}

}


