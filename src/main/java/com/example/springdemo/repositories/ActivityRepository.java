package com.example.springdemo.repositories;

import com.example.springdemo.entities.ActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<ActivityEntity, Integer> {

}
