package com.example.springdemo.RabbitMQ;

import com.example.springdemo.entities.ActivityEntity;
import com.example.springdemo.repositories.ActivityRepository;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Component
public class Consumer implements MessageListener {

    @Autowired
    ActivityRepository activityRepository;



    @Override

    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message){
        ActivityEntity activityEntity = new ActivityEntity();
        String msg = new String(message.getBody());
        System.out.println("Received Message:");
        msg = msg.replace("\\t"," ");
        msg = msg.replace("\"","");
        String[] parts= msg.split("\\s\\s");
        System.out.println("\nStart time:--"+parts[0]+"--");
        System.out.println("End time:--"+parts[1]+"--");
        System.out.println("Activity:--"+parts[2]+"--");

        activityEntity.setName(parts[2]);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date startDate, endDate;


        try {
            startDate = dateFormat.parse(parts[0]);
            activityEntity.setStart_time(startDate);

            endDate = dateFormat.parse(parts[1]);
            activityEntity.setStart_time(endDate);

            if (parts[2].trim().equals("Sleeping")){
                if (TimeUnit.MILLISECONDS.toHours(endDate.getTime()-startDate.getTime())>12)
                    activityEntity.setFlag(1);
                    System.out.println("\nRule 1 broken!\n");
            }
            else if (parts[2].trim().equals("Toileting")){
                if (TimeUnit.MILLISECONDS.toHours(endDate.getTime()-startDate.getTime())>1)
                    activityEntity.setFlag(1);
                System.out.println("\nRule 2 broken!\n");
            }
            else if (parts[2].trim().equals("Leaving")){
                if (TimeUnit.MILLISECONDS.toHours(endDate.getTime()-startDate.getTime())>12)
                    activityEntity.setFlag(1);
                System.out.println("\nRule 3 broken!\n");
            }

            activityRepository.save(activityEntity);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}

